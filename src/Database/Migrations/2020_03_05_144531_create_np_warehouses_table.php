<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('np_warehouses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('SiteKey');
            $table->string('Description');
            $table->string('DescriptionRu');
            $table->string('Phone');
            $table->string('TypeOfWarehouse');
            $table->string('Ref')->unique();
            $table->string('Number')->nullable();
            $table->string('CityRef')->nullable();
            $table->string('CityDescription')->nullable();
            $table->string('CityDescriptionRu')->nullable();
            $table->string('Longitude')->nullable();
            $table->string('Latitude')->nullable();
            $table->string('PostFinance')->nullable();
            $table->string('BicycleParking')->nullable();
            $table->string('POSTerminal')->nullable();
            $table->string('InternationalShipping')->nullable();
            $table->string('TotalMaxWeightAllowed')->nullable();
            $table->string('PlaceMaxWeightAllowed')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('np_warehouses');
    }
};
