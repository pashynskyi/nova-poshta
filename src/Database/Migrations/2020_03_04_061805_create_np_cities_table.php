<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('np_cities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('Description');
            $table->string('DescriptionRu');
            $table->string('Ref')->unique();
            $table->string('Delivery1')->nullable();
            $table->string('Delivery2')->nullable();
            $table->string('Delivery3')->nullable();
            $table->string('Delivery4')->nullable();
            $table->string('Delivery5')->nullable();
            $table->string('Delivery6')->nullable();
            $table->string('Delivery7')->nullable();
            $table->string('Area')->nullable();
            $table->string('SettlementType')->nullable();
            $table->string('IsBranch')->nullable();
            $table->string('PreventEntryNewStreetsUser')->nullable();
            $table->string('Conglomerates')->nullable();
            $table->string('CityID')->nullable();
            $table->string('SettlementTypeDescriptionRu')->nullable();
            $table->string('SettlementTypeDescription')->nullable();
            $table->string('SpecialCashCheck')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('np_cities');
    }
};
