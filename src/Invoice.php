<?php

namespace Pashynskyi\NovaPoshta;

class Invoice
{
    protected $api;

    public $properties = [
        "NewAddress" => "1",
        "PayerType" => "Recipient",
        "PaymentMethod" => "Cash",
        "CargoType" => "Parcel",
        "VolumeGeneral" => '*',
        "Weight" => "*",
        "ServiceType" => "WarehouseWarehouse",
        "SeatsAmount" => "1",
        "Description" => "*",
        "Cost" => "*",
        "Sender" => "*",
        "CitySender" => "*",
        "SenderAddress" => "*",
        "ContactSender" => "*",
        "SendersPhone" => "*",
        "RecipientArea" => "",
        "RecipientAreaRegions" => "",
        "CityRecipient" => "*",
        "RecipientAddress" => "*",
        "RecipientHouse" => "",
        "RecipientFlat" => "",
        "RecipientName" => "*",
        "RecipientType" => "PrivatePerson",
        "RecipientsPhone" => "*",
    ];

    public function __construct($apiKey)
    {
        $this->api = new API($apiKey);
    }

    public function setBackwardDeliveryPrice($price)
    {
        return $this->setProperty('BackwardDeliveryData', [
            [
                "PayerType" => "Recipient",
                "CargoType" => "Money",
                "RedeliveryString" => $price,
            ]
        ]);
    }

    public function setPayerType($value)
    {
        return $this->setProperty('PayerType', $value);
    }

    public function setVolumeGeneral($value) // Объем общий, м.куб
    {
        return $this->setProperty('VolumeGeneral', $value);
    }

    public function setWeight($value)
    {
        return $this->setProperty('Weight', $value);
    }

    public function setSeatsAmount($value)
    {
        return $this->setProperty('SeatsAmount', $value);
    }

    public function setDescription($value)
    {
        return $this->setProperty('Description', $value);
    }

    public function setCost($value)
    {
        return $this->setProperty('Cost', $value);
    }

    public function setSender($value)
    {
        return $this->setProperty('Sender', $value);
    }

    public function setCitySender($value)
    {
        return $this->setProperty('CitySender', $value);
    }

    public function setSenderAddress($value)
    {
        return $this->setProperty('SenderAddress', $value);
    }

    public function setContactSender($value)
    {
        return $this->setProperty('ContactSender', $value);
    }

    public function setSendersPhone($value)
    {
        return $this->setProperty('SendersPhone', $value);
    }

    public function setCityRecipient($value)
    {
        return $this->setProperty('CityRecipient', $value);
    }

    public function setRecipientAddress($value)
    {
        return $this->setProperty('RecipientAddress', $value);
    }

    public function setRecipientName($value)
    {
        return $this->setProperty('RecipientName', $value);
    }

    public function setRecipientsPhone($value)
    {
        return $this->setProperty('RecipientsPhone', $value);
    }

    public function setProperty($key, $value)
    {
        $this->properties[$key] = $value;
        return $this;
    }

    public function send()
    {
        $np = $this->api->send('InternetDocument', 'save', $this->properties);

        if ($np->isErrors()) {
            throw new \Exception('NP API errors: ' . implode(', ', $np->getErrors()));
        }

        return $np->getData();
    }
}
