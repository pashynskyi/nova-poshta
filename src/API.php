<?php

namespace Pashynskyi\NovaPoshta;

class API
{
    const SERVER = 'http://api.novaposhta.ua/v2.0/json';

    private $apiKey;

    protected $errors = [];
    protected $warnings = [];
    protected $data = [];

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function send($modelName, $calledMethod, array $methodProperties = [])
    {
        $headers = [
            'body' => json_encode([
                'apiKey' => $this->apiKey,
                'modelName' => $modelName,
                'calledMethod' => $calledMethod,
                'methodProperties' => $methodProperties,
            ]),
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ];
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', static::SERVER . "/{$modelName}/{$calledMethod}/", $headers);
            $responseBody = mb_convert_encoding($response->getBody(), 'utf-8', 'auto');
            $jsonResponse = json_decode($responseBody, true);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }

        if (isset($jsonResponse['errors'])) {
            $this->errors = $jsonResponse['errors'];
        }

        if (isset($jsonResponse['warnings'])) {
            $this->warnings = $jsonResponse['warnings'];
        }

        if (isset($jsonResponse['data'])) {
            $this->data = $jsonResponse['data'];
        }

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }

    public function isErrors()
    {
        return !empty($this->errors);
    }
}
