<?php

namespace Pashynskyi\NovaPoshta\Console\Commands;

use Illuminate\Console\Command;
use Pashynskyi\NovaPoshta\Saver;

class Sync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'np:sync {--entity=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync np data from api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $np = new Saver(config('app.np_api_key'));
        $entity = $this->option('entity');

        if (empty($entity)) {
            $np->saveAreas();
            echo "Areas saved\n";
            $np->saveCities();
            echo "Cities saved\n";
            $np->saveWarehouses();
            echo "Warehouses saved\n";
            return;
        }

        switch ($entity) {
            case 'a': {
                $np->saveAreas();
                echo "Areas saved\n";
                break;
            }
            case 'c': {
                $np->saveCities();
                echo "Cities saved\n";
                break;
            }
            case 'w': {
                $np->saveWarehouses();
                echo "Warehouses saved\n";
                break;
            }
            default: {
                echo "Invalid entity\n";
                break;
            }
        }
    }
}
