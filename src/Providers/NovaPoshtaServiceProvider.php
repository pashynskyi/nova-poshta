<?php

namespace Pashynskyi\NovaPoshta\Providers;

use Illuminate\Support\ServiceProvider;
use Pashynskyi\NovaPoshta\Console\Commands\Sync;

class NovaPoshtaServiceProvider extends ServiceProvider
{
    const PATH_MIGRATIONS = __DIR__ . '/../Database/Migrations';
    const PATH_ROUTES = __DIR__ . '/../Routes/web.php';

    public function register()
    {
        //
    }

    public function boot()
    {
        $this->loadMigrationsFrom(self::PATH_MIGRATIONS);

        $this->loadRoutesFrom(self::PATH_ROUTES);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                self::PATH_MIGRATIONS => $this->app->databasePath('vendor/nova-poshta'),
            ], 'migrations');

            $this->commands([
                Sync::class,
            ]);
        }
    }
}
