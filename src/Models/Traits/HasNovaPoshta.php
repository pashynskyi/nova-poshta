<?php

namespace Pashynskyi\NovaPoshta\Models\Traits;

use Pashynskyi\NovaPoshta\Models\City;
use Pashynskyi\NovaPoshta\Models\Warehouse;

trait HasNovaPoshta
{
    public function npCity()
    {
        return $this->belongsTo(City::class, 'np_city_ref', 'Ref');
    }

    public function npWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'np_warehouse_ref', 'Ref');
    }
}
