<?php

namespace Pashynskyi\NovaPoshta\Models;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model
{
    protected $table = 'np_contact_persons';

    protected $fillable = [
        'ApiToken',
        'Description',
        'Phones',
        'Email',
        'CounterpartyRef',
        'ContactPersonRef',
        'RecipientType',
        'LastName',
        'FirstName',
        'MiddleName',
        'MarketplacePartnerDescription',
    ];
}
