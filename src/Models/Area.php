<?php

namespace Pashynskyi\NovaPoshta\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'np_areas';

    protected $fillable = [
        'Description',
        'Ref',
        'AreasCenter',
    ];
}
