<?php

namespace Pashynskyi\NovaPoshta\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'np_cities';

    protected $fillable = [
        'Description',
        'DescriptionRu',
        'Ref',
        'Delivery1',
        'Delivery2',
        'Delivery3',
        'Delivery4',
        'Delivery5',
        'Delivery6',
        'Delivery7',
        'Area',
        'SettlementType',
        'IsBranch',
        'PreventEntryNewStreetsUser',
        'Conglomerates',
        'CityID',
        'SettlementTypeDescriptionRu',
        'SettlementTypeDescription',
        'SpecialCashCheck',
    ];

    public function warehouses()
    {
        return $this->hasMany(Warehouse::class, 'CityRef', 'Ref');
    }

    public static function descriptionField()
    {
        return app()->getLocale() == 'ru' ? 'DescriptionRu' : 'Description';
    }

    public function description()
    {
        return $this->{static::descriptionField()};
    }
}
