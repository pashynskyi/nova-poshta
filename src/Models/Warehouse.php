<?php

namespace Pashynskyi\NovaPoshta\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'np_warehouses';

    protected $fillable = [
        'SiteKey',
        'Description',
        'DescriptionRu',
        'Phone',
        'TypeOfWarehouse',
        'Ref',
        'Number',
        'CityRef',
        'CityDescription',
        'CityDescriptionRu',
        'Longitude',
        'Latitude',
        'PostFinance',
        'BicycleParking',
        'POSTerminal',
        'InternationalShipping',
        'TotalMaxWeightAllowed',
        'PlaceMaxWeightAllowed',
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'CityRef', 'Ref');
    }

    public static function getCityWarehouses($warehouseRef)
    {
        $cityWarehouses = [];
        if ($warehouseRef) {
            $warehouse = static::whereRef($warehouseRef)->with('city')->firstOrNew();
            $cityWarehouses = isset($warehouse->city->warehouses) ? $warehouse->city->warehouses->pluck(static::descriptionField(), 'Ref') : [];
        }
        return $cityWarehouses;
    }

    public static function descriptionField()
    {
        return app()->getLocale() == 'ru' ? 'DescriptionRu' : 'Description';
    }

    public function description()
    {
        return $this->{static::descriptionField()};
    }
}
