<?php

namespace Pashynskyi\NovaPoshta\Controllers;

use Pashynskyi\NovaPoshta\Models\City;
use Illuminate\Http\Request;
use Pashynskyi\NovaPoshta\Models\Warehouse;

class WarehouseController extends BaseController
{
    public function warehouses(Request $request)
    {
        $cityRef = $request->get('city_ref');
        $results = [];

        if (!empty($cityRef)) {
            $descriptionField = Warehouse::descriptionField();
            $results = Warehouse::where('CityRef', $cityRef)->orderBy('TypeOfWarehouse')->orderBy('Number')->pluck($descriptionField, 'Ref');
        }

        return ['items' => $results];
    }
}
