<?php

namespace Pashynskyi\NovaPoshta\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;;

class BaseController extends Controller
{
    public function __construct(Request $request)
    {
        if ($request->header('locale')) {
            app()->setLocale($request->header('locale'));
        }
    }
}
