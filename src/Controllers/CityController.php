<?php

namespace Pashynskyi\NovaPoshta\Controllers;

use Pashynskyi\NovaPoshta\Models\City;
use Illuminate\Http\Request;

class CityController extends BaseController
{
    public function cities(Request $request)
    {
        $search = $request->get('search');
        $search = trim($search);
        $results = [];

        if (!empty($search) && mb_strlen($search) > 1) {
            $descriptionField = City::descriptionField();
            $results = City::where($descriptionField, 'like', "{$search}%")->get()->pluck($descriptionField, 'Ref');
        }

        return ['items' => $results];
    }
}
