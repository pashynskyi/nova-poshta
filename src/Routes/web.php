<?php
Route::group(['as' => 'np.', 'prefix' => 'np', 'namespace' => '\\Pashynskyi\\NovaPoshta\\Controllers', 'middleware' => ['web']], function () {
    Route::get('/cities', 'CityController@cities')->name('cities');
    Route::get('/warehouses', 'WarehouseController@warehouses')->name('warehouses');
});
