<?php

namespace Pashynskyi\NovaPoshta;

use Pashynskyi\NovaPoshta\Models\Area;
use Pashynskyi\NovaPoshta\Models\City;
use Pashynskyi\NovaPoshta\Models\Warehouse;

class Saver
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function saveAreas()
    {
        return $this->save(Area::class, 'AddressGeneral', 'getAreas');
    }

    public function saveCities()
    {
        return $this->save(City::class, 'AddressGeneral', 'getCities');
    }

    public function saveWarehouses()
    {
        return $this->save(Warehouse::class, 'AddressGeneral', 'getWarehouses');
    }

    protected function save($modelClass, $modelName, $calledMethod)
    {
        $modelClass::truncate();

        $np = new API($this->apiKey);
        $np->send($modelName, $calledMethod, ['Warehouse' => 1]);

        if ($np->isErrors()) {
            throw new \Exception('NP API errors: ' . implode(', ', $np->getErrors()));
        }

        foreach ($np->getData() as $item) {
            $modelClass::create((array)$item);
        }
        sleep(2); // !important

        return $this;
    }
}
